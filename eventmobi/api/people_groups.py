from .base import EventMobiApiBase


class PeopleGroups(EventMobiApiBase):

    def get_group_detail(self, event_id, group_id):
        """
        Get all People Group's Details

        :param str event_id:
        :param str group_id:
        :return: It returns a detail response containing the Group Response Model for a given people group identified
        by its unique id (GROUP_ID).
        """
        url = self._get_people_groups_endpoint(event_id, group_id)
        return self._request('get', url)

    def get_groups(self, event_id):
        """
        Get all People Groups in Event

        :param str event_id: Event ID
        :return: Returns a collection response. Each entry on the list contains a Group Response Model.
        :rtype: dict
        """
        url = self._get_people_groups_endpoint(event_id)
        return self._request('get', url)

    def add_group(self, event_id, name):
        """
        Add a People Group

        :param str event_id:
        :param str name:
        :return: It returns a detail response containing group details
        :rtype: dict
        """

        url = self._get_people_groups_endpoint(event_id)
        data = {
            'name': name,
        }
        return self._request('post', url, data=data)

    def delete_group(self, event_id, group_id):
        """
        Remove a People Groups from Event

        The group should be identified by its unique id (GROUP_ID).

        :param event_id:
        :param person_id:
        :return: It returns a detail response containing the Group Response Model of the deleted group.
        """
        url = self._get_people_groups_endpoint(event_id, group_id)
        return self._request('delete', url)

    def get_persons_groups(self, event_id, person_id):
        """
        Get all Groups a Person is a Member of

        :param event_id:
        :param person_id:
        :return: Returns a collection response. Each entry on the list contains a Group Response Model.
        :rtype: list
        """
        url = self._get_people_endpoint(event_id, person_id)
        url += '/groups'

        return self._request('get', url)


