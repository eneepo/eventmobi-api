from .base import EventMobiApiBase


class SessionRoles(EventMobiApiBase):

    def get_role(self, event_id, role_id):
        """
        Get a Role's Details

        :param str event_id:
        :param str role_id:
        :return:
         its unique id (SESSION_ID).
        """
        url = self._get_sessions_roles_endpoint(event_id, role_id)
        return self._request('get', url)

    def get_session_roles(self, event_id):
        """
         Get all Session Roles

        :param str event_id: Event ID
        :return: Returns a collection response. Each entry on the list contains a Role Response Model.
        :rtype: dict
        """
        url = self._get_sessions_roles_endpoint(event_id)
        return self._request('get', url)

    def add_session(self, event_id, name):
        """
         Create a new Session Role

         Add a new session to the event, as represented by the given Role Request Model.

         :param str event_id:
         :param str name:
         :return: It returns a detail response containing the Role Response Model of the added session.
         :rtype: dict

         """

        data = {
            'name': name
        }

        url = self._get_sessions_roles_endpoint(event_id)

        return self._request('post', url, data=data)

    def update_session(self, event_id, role_id, name):
        """
        Change a Role's Details

        :param str event_id:
        :param str role_id:
        :param str name:
        :return: It returns a detail response containing the Session Response Model of the modified person.
        :rtype: dict

        """

        data = {
            'name': name
        }

        url = self._get_sessions_roles_endpoint(event_id, role_id)

        return self._request('patch', url, data=data)

    def delete_session(self, event_id, role_id):
        """
        Removes a Session Role from the event and delete its data.

        The session role should be identified by its unique id (role_id).

        :param event_id:
        :param role_id:
        :return: It returns a detail response containing the Role Response Model of the deleted session role.
        """
        url = self._get_sessions_endpoint(event_id, role_id)
        return self._request('delete', url)

