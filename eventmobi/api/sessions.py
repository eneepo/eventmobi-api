from eventmobi.exceptions import InvalidParameterException
from .base import EventMobiApiBase


class Sessions(EventMobiApiBase):

    def get_session(self, event_id, session_id):
        """
        Get a Session's Details

        :param str event_id:
        :param str session_id:
        :return: It returns a detail response containing the Session Response Model for a given Session identified by
         its unique id (SESSION_ID).
        """
        url = self._get_sessions_endpoint(event_id, session_id)
        return self._request('get', url)

    def get_sessions(self, event_id):
        """
         Get all Sessions in Event

        :param str event_id: Event ID
        :return: Returns a collection response. Each entry on the list contains a Session Response Model.
        :rtype: dict
        """
        url = self._get_sessions_endpoint(event_id)
        return self._request('get', url)

    def add_session(self, event_id, **kwargs):
        """
         Add a Sessio

         Add a new session to the event, as represented by the given Session Request Model.

         :param str event_id:
         :param str person_id:
         **kwargs: Arbitrary keyword arguments.
         :return: It returns a detail response containing the Session Response Model of the added session.
         :rtype: dict

         """

        params = ['name', 'description', 'location', 'start_datetime', 'end_datetime', 'track_ids', 'tracks', 'roles', ]
        data = {}
        for key in kwargs:
            if key not in params:
                raise InvalidParameterException
            else:
                data[key] = kwargs[key]

        url = self._get_sessions_endpoint(event_id)

        return self._request('post', url, data=data)

    def update_session(self, event_id, session_id, **kwargs):
        """
        Change/Update fields for a given session identified by its unique id (SESSION_ID).

        You request should contains only the fields you desire to modify and should be a sub set of
        the Session Request Model.

        :param str event_id:
        :param str session_id:
        **kwargs: Arbitrary keyword arguments.
        :return: It returns a detail response containing the Session Response Model of the modified person.
        :rtype: dict

        """

        params = ['name', 'description', 'location', 'start_datetime', 'end_datetime', 'track_ids', 'tracks', 'roles', ]
        data = {}
        for key in kwargs:
            if key not in params:
                raise InvalidParameterException
            else:
                data[key] = kwargs[key]

        url = self._get_sessions_endpoint(event_id, session_id)

        return self._request('patch', url, data=data)

    def delete_session(self, event_id, session_id):
        """
        Removes a Session from the event and delete its data.

        The session should be identified by its unique id (SESSION_ID).

        :param event_id:
        :param session_id:
        :return: It returns a detail response containing the Session Response Model of the deleted session.
        """
        url = self._get_sessions_endpoint(event_id, session_id)
        return self._request('delete', url)

    def add_person_to_session(self, event_id, session_id, role_id, people_ids):
        """
        Add Person to Existing Session

        To add a person to an existing session, you can use this method for convenience.

        :param str event_id:
        :param str session_id:
        :param str role_id:
        :param list people_ids:
        :return: Returns people_ids
        :rtype: list
        """
        url = self._get_sessions_endpoint(event_id, session_id)
        url += '/roles/{role_id}/people'.format(role_id=role_id)
        data = {
            'people_ids': people_ids
        }

        return self._request('post', url, data=data)


