import json
import requests
from urllib.parse import urljoin

from ..exceptions import EventMobiException, InvalidRequestMethodException


class EventMobiApiBase:
    """
    Wrapper for EventMobi  API
    https://developer.eventmobi.com/
    """
    _api_key = None
    _api_base_url = 'https://api.eventmobi.com/v2/'

    def __init__(self, api_key, api_url=None):
        self._api_key = api_key

        if api_url:
            self.api_url = api_url

    def _request(self, method, url, headers={}, **kwargs):
        headers['X-API-KEY'] = self._api_key

        if method == 'get':
            params = kwargs.get('params', {})
            response = requests.get(url, headers=headers, params=params)

        elif method == 'post':
            data = kwargs.get('data', {})
            response = requests.post(url, headers=headers, json=data)

        elif method == 'patch':
            data = kwargs.get('data', {})
            response = requests.patch(url, headers=headers, json=data)

        elif method == 'delete':
            response = requests.delete(url, headers=headers)

        else:
            raise InvalidRequestMethodException

        if response.status_code not in [200, 201]:
            response_json = response.json()
            raise EventMobiException(response.status_code, response_json['errors'])

        return response

    def _get_people_endpoint(self, event_id, person_id=''):
        endpoint = "events/{event_id}/people/resources".format(event_id=event_id)
        if person_id != '':
            endpoint += '/' + person_id
        return urljoin(self._api_base_url, endpoint)

    def _get_people_groups_endpoint(self, event_id, group_id=''):
        endpoint = "events/{event_id}/people/groups".format(event_id=event_id)
        if group_id != '':
            endpoint += '/' + group_id
        return urljoin(self._api_base_url, endpoint)

    def _get_sessions_endpoint(self, event_id, session_id=''):
        endpoint = "events/{event_id}/sessions/resources".format(event_id=event_id)
        if session_id != '':
            endpoint += '/' + session_id
        return urljoin(self._api_base_url, endpoint)

    def _get_sessions_roles_endpoint(self, event_id, role_id=''):
        endpoint = "events/{event_id}/sessions/roles".format(event_id=event_id)
        if role_id != '':
            endpoint += '/' + role_id
        return urljoin(self._api_base_url, endpoint)

    def _get_sessions_tracks_endpoint(self, event_id, track_id=''):
        endpoint = "events/{event_id}/sessions/tracks".format(event_id=event_id)
        if track_id != '':
            endpoint += '/' + track_id
        return urljoin(self._api_base_url, endpoint)
