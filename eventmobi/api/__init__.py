from .people import People
from .people_groups import PeopleGroups
from .sessions import Sessions
from .session_roles import SessionRoles


class EventMobi(People, PeopleGroups, Sessions, SessionRoles):
    pass
