from eventmobi.exceptions import InvalidParameterException
from .base import EventMobiApiBase


class People(EventMobiApiBase):

    def get_person(self, event_id, person_id):
        """
        Get a Person's Details

        :param str event_id:
        :param str person_id:
        :return: It returns a detail response containing the Person Response Model for a given person identified by its
            unique id (PERSON_ID).
        """
        url = self._get_people_endpoint(event_id, person_id)
        return self._request('get', url)

    def get_people(self, event_id):
        """
        Get All People in Event

        :param str event_id: Event ID
        :return: A collection response. Each entry on the list contains a Person Response Model.
        :rtype: dict
        """
        url = self._get_people_endpoint(event_id)
        return self._request('get', url)

    def add_person(self, event_id, first_name, last_name, email='', company='', title='', about='',
                   group_ids=[], session_roles=[], custom_fields={}, settings={},
                   upload_image_url=None):
        """
        Add a new person to the event, as represented by the given Person Request Model.

        :param str event_id:
        :param str first_name:
        :param str last_name:
        :param str email:
        :param str company:
        :param str title:
        :param str about:
        :param list group_ids:
        :param list session_roles:
        :param dict custom_fields:
        :param settings:
        :param upload_image_url:
        :type upload_image_url: str or None
        :return: A detail response containing the Person Response Model of the added person.
        :rtype: dict
        """

        url = self._get_people_endpoint(event_id)
        data = {
            'first_name': first_name,
            'last_name': last_name,
            'email': email,
            'company': company,
            'title': title,
            'about': about,
            'group_ids': group_ids,
            'session_roles': session_roles,
            'custom_fields': custom_fields,
            'settings': settings,
            'upload_image_url': upload_image_url
        }
        return self._request('post', url, data=data)

    def update_person(self, event_id, person_id, **kwargs):
        """
        Change/Update fields for a given person identified by its unique id (PERSON_ID).

        You request should contains only the fields you desire to modify and should be a sub set of
        the Person Request Model.

        :param str event_id:
        :param str person_id:
        **kwargs: Arbitrary keyword arguments.
        :return: A detail response containing the Person Response Model of the added person.
        :rtype: dict

        """

        params = ['first_name', 'last_name', 'email', 'company', 'title', 'about', 'grop_ids', 'session_roles',
                  'custom_fields', 'settings', 'upload_image_url', ]
        data = {}
        for key in kwargs:
            if key not in params:
                raise InvalidParameterException
            else:
                data[key] = kwargs[key]

        url = self._get_people_endpoint(event_id, person_id)

        return self._request('patch', url, data=data)

    def delete_person(self, event_id, person_id):
        """
        Removes a Person from the event and delete its data.

        The person should be identified by its unique id (PERSON_ID).

        :param event_id:
        :param person_id:
        :return: A detail response containing the Person Response Model of the deleted person.
        """
        url = self._get_people_endpoint(event_id, person_id)
        return self._request('delete', url)

    def add_session_to_persons_agenda(self, event_id, person_id, role_id, session_ids):
        """
        If you have an existing person and you want to add them as a specific role in a session, you can use
        this method.

        Example: Add person as Volunteer in a session

        :param event_id:
        :param person_id:
        :param role_id:
        :param session_ids:
        :return: Returns session_ids
        :rtype: list
        """
        url = self._get_people_endpoint(event_id, person_id)
        url += '/roles/{role_id}/sessions'.format(role_id=role_id)
        data = {
            'session_ids': session_ids
        }

        return self._request('post', url, data=data)


