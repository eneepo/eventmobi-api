from eventmobi.exceptions import InvalidParameterException
from .base import EventMobiApiBase


class SessionTracks(EventMobiApiBase):

    def get_track(self, event_id, track_id):
        """
        Get a Role's Details

        :param str event_id:
        :param str track_id:
        :return:
         its unique id (TRACK_ID).
        """
        url = self._get_sessions_tracks_endpoint(event_id, track_id)
        return self._request('get', url)

    def get_session_tracks(self, event_id):
        """
         Get all Tracks

        :param str event_id: Event ID
        :return:
        :rtype: dict
        """
        url = self._get_sessions_tracks_endpoint(event_id)
        return self._request('get', url)

    def add_track(self, event_id, **kwargs):
        """
        Create a new Track

        :param str event_id:
        **kwargs: Arbitrary keyword arguments.
        :return: It returns a detail response containing the Track of that was added.
        :rtype: dict

         """

        params = ['name', 'color']
        data = {}
        for key in kwargs:
            if key not in params:
                raise InvalidParameterException
            else:
                data[key] = kwargs[key]

        url = self._get_sessions_tracks_endpoint(event_id)

        return self._request('post', url, data=data)

    def update_session(self, event_id, track_id, name):
        """
        Updates a track.

        :param str event_id:
        :param str track_id:
        :param str name:
        :return: It returns a detail response containing the Track of that was added.
        :rtype: dict

        """

        data = {
            'name': name
        }

        url = self._get_sessions_tracks_endpoint(event_id, track_id)

        return self._request('patch', url, data=data)

    def delete_track(self, event_id, track_id):
        """
        Delete a Track


        :param event_id:
        :param track_id:
        :return: It returns a detail response containing the Track Response Model of the deleted session role.
        """
        url = self._get_sessions_tracks_endpoint(event_id, track_id)
        return self._request('delete', url)

