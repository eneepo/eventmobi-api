class EventMobiException(Exception):
    def __init__(self, code, message):
        self.status_code = code
        self.message = message


class InvalidParameterException(Exception):
    pass


class InvalidRequestMethodException(Exception):
    pass

